"""inventario_2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import  settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    # insert -----------------------------------------------------------------------------------------
    url(r'^art_insert$', "inventory.views.Ingreso_articulo", name='i_articulo'),
    url(r'^sol_insert', "inventory.views.Ingreso_solicitante", name='i_solicitante'),
    url(r'^pres_insert', "inventory.views.Ingreso_prestamo", name='i_prestamo'),
    # listar -----------------------------------------------------------------------------------------
    url(r'^list_vig$', "inventory.views.Listar_prestamo_v", name='l_prestamov'),
    url(r'^list_prestamo$', "inventory.views.Listar_prestamo", name='l_prestamo'),
    url(r'^list_solicitante$', "inventory.views.Listar_solicitante", name='l_solicitante'),
    url(r'^list_articulo$', "inventory.views.Listar_articulo", name='l_articulo'),
    url(r'^list_prestamo_$', "inventory.views.Listar_prestamo1", name='li_prestamo'),
    url(r'^list_solicitante_$', "inventory.views.Listar_solicitante1", name='li_solicitante'),
    url(r'^list_articulo_$', "inventory.views.Listar_articulo1", name='li_articulo'),
    # login -------------------------------------------------------------------------------------------
    url(r'^confir/$', "inventory.views.login_val", name='login_val'),
    url(r'^$', "inventory.views.inicio", name='inicio'),
    url(r'^login/$', "inventory.views.login", name='login'),
    url(r'^logout/$', "inventory.views.salir", name='logout'),
    url(r'^home$', "inventory.views.inicio", name='home'),
    url(r'^about$', "inventory.views.about", name='about'),
    # editar ------------------------------------------------------------------------------------------
    url(r'^edit/articulo/(?P<codigo>\d+)/$', "inventory.views.edit_art", name='edit_art'),
    url(r'^edit/solicitante/(?P<codigo>\d+)/$', "inventory.views.edit_solicitante", name='edit_solicitante'),
    url(r'^dev/(?P<codigo>\d+)/$', "inventory.views.devolver", name='devolver'),
    url(r'^edit/articulo$', "inventory.views.edit_list_articulo", name='edit_list_articulo'),
    url(r'^edit/prestamo$', "inventory.views.edit_list_prestamo", name='edit_list_prestamo'),
    # eliminar ----------------------------------------------------------------------------------------
    url(r'^eliminar/solicitante/(?P<codigo>\d+)/$', "inventory.views.delete_solicitante", name='eliminar'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)