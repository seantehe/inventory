from django.contrib import admin
from .models import Ingresar
from .forms import Ingresado_form

# Register your models here.
class AdminIngresar(admin.ModelAdmin):
    list_display = ["__unicode__","nombre","marca","ingreso","update"]
    #class Meta:
    #    model = Ingresar
    form = Ingresado_form

admin.site.register(Ingresar, AdminIngresar)
