from django import forms
from .models import Ingresar

class Ingresado_form(forms.ModelForm):
    class Meta:
        model = Ingresar
        fields = ["nombre","marca","codigo"]

    def clean_nombre(self):
        info = self.cleaned_data
        if info.get("nombre") == "seba":
            return "yo"
        return info.get("nombre")

    def clean_marca(self):
        info = self.cleaned_data
        if info.get("marca") == "nike":
            raise forms.ValidationError("esta marca no se puede ocupar")
        return info.get("marca")


class RegForm(forms.Form):
    codigo = forms.CharField(max_length=120)
    nombre_form = forms.CharField(max_length=120)
    marca = forms.CharField(max_length=120)