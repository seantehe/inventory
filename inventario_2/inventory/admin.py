from django.contrib import admin
from .forms import Articulo_form
from .models import Articulo
from .forms import Solicitante_form
from .models import Solicitante
from .forms import Prestamo_form
from .models import Prestamo

# Register your models here.
class AdminArticulo(admin.ModelAdmin):
    # class Meta:
    #     model = Articulo
    list_display = ["__unicode__","nombre","marca","descripcion","ingreso","update"]
    form = Articulo_form

class AdminSolicitante(admin.ModelAdmin):
    # class Meta:
    #     model = Solicitante
    list_display = ["__unicode__","nombre","apellido","alias","area","telefono"]
    form = Solicitante_form

class AdminPrestamo(admin.ModelAdmin):
    # class Meta:
    #     model = Prestamo
    list_display = ["codigo_art","id_solicitante","descripcion"]
    form = Prestamo_form

admin.site.register(Articulo , AdminArticulo)
admin.site.register(Solicitante , AdminSolicitante)
admin.site.register(Prestamo , AdminPrestamo)