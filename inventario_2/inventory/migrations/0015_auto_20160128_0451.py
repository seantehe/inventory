# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-28 04:51
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0014_auto_20160127_2115'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solicitante',
            name='rut',
            field=models.PositiveIntegerField(primary_key=True, serialize=False, validators=[django.core.validators.MaxValueValidator(99999999)]),
        ),
    ]
