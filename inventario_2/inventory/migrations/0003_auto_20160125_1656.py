# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-25 16:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0002_articulo_descripcion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articulo',
            name='descripcion',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='prestamo',
            name='descripcion',
            field=models.TextField(),
        ),
    ]
