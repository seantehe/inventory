from django.shortcuts import render
from .forms import Articulo_form
from .forms import Articulo_edit_form
from .models import Articulo
from .forms import Solicitante_form
from .forms import Solicitante_edit_form
from .models import Solicitante
from .forms import Prestamo_form
from .models import Prestamo
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import logout
from django.shortcuts import render_to_response
from django.template import RequestContext
# Create your views here.


#------------------------------------------------------------------------------------------
# los ingresos de datos
def Ingreso_articulo(request):

    if request.user.is_authenticated():
        usuario = "%s" %(request.user)
        form = Articulo_form(request.POST or None)
        context = {
            "pos": "articulo",
            "usuario": usuario,
            "form": form
        }
        if form.is_valid():
            info = form.save(commit=False)
            info.en_prestamo = 0
            info.user_ingre = str(usuario)
            info.user_update = str(usuario)
            info.save()
            return HttpResponseRedirect(reverse('i_articulo'))
        return render(request,"ingreso.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

def Ingreso_solicitante(request):

    if request.user.is_authenticated():
        usuario = "%s" %(request.user)
        form = Solicitante_form(request.POST or None)
        context = {
            "pos": "solicitante",
            "usuario": usuario,
            "form": form
        }
        if form.is_valid():
            info = form.save(commit=True)
            print info.nombre
            return HttpResponseRedirect(reverse('i_solicitante'))
        return render(request,"ingreso.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))


def Ingreso_prestamo(request):

    if request.user.is_authenticated():
        print "hola1"
        usuario = "%s" %(request.user)
        form = Prestamo_form(request.POST or None)
        context = {
            "pos": "prestamo",
            "usuario": usuario,
            "form": form
        }
        if form.is_valid():
            info = form.save(commit=False)
            codigo = str(info.codigo_art).split("-")
            art = Articulo.objects.get(codigo=codigo[0])
            if info.cantidad > (art.cantidad - art.en_prestamo):
                messages.success(request, "no existe stock para la cantidad solicitada")
                return HttpResponseRedirect(reverse('i_prestamo'))
            else:
                art.en_prestamo = art.en_prestamo + info.cantidad
                art.save()
                info.user_ingre = str(usuario)
                info.user_update = str(usuario)
                info.estado = True
                info.save()
                messages.success(request, "prestamo realizado")
                return HttpResponseRedirect(reverse('i_prestamo'))
        return render(request,"ingreso.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

#------------------------------------------------------------------------------------------
# los listar de datos


def Listar_prestamo(request):
    if request.user.is_authenticated():
        queryset = Prestamo.objects.all()
        context = {
            "pos": "prestamo",
            "queryset": queryset
        }
        return render(request, "listar_produc.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))
def Listar_prestamo_v(request):
    if request.user.is_authenticated():
        queryset = Prestamo.objects.all()
        context = {
            "pos": "prestamo",
            "queryset": queryset
        }
        return render(request, "list_vigentes.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

def Listar_solicitante(request):
    if request.user.is_authenticated():
        queryset = Solicitante.objects.all()
        context = {
            "pos": "solicitante",
            "queryset": queryset
        }
        return render(request, "listar_produc.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

def Listar_articulo(request):
    if request.user.is_authenticated():
        queryset = Articulo.objects.all()
        context = {
            "pos": "articulo",
            "queryset": queryset
        }
        return render(request, "listar_produc.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))



def Listar_prestamo1(request):
    if request.user.is_authenticated():
        queryset = Prestamo.objects.all()
        context = {
            "pos": "prestamo",
            "queryset": queryset
        }
        return render(request, "list_ingre.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

def Listar_solicitante1(request):
    if request.user.is_authenticated():
        queryset = Solicitante.objects.all()
        context = {
            "pos": "solicitante",
            "queryset": queryset
        }
        return render(request, "list_ingre.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

def Listar_articulo1(request):
    if request.user.is_authenticated():
        queryset = Articulo.objects.all()
        context = {
            "pos": "articulo",
            "queryset": queryset
        }
        return render(request, "list_ingre.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

#------------------------------------------------------------------------------------------
# login y acceso


def login_val(request):
    username = request.GET["login-name"]
    password = request.GET["login-pass"]
    print "pase"
    user = authenticate(username=username, password=password)
    print "pase2"
    if user is not None:
        print "pase3"
        auth_login(request, user)
        print "pase4"
        return HttpResponseRedirect(reverse('home'))
    else:
        print "malo"
        messages.success(request, "usuario no registrado o pass incorrecta")
        return HttpResponseRedirect(reverse('inicio'))

def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('home'))
    else:
        context = {
            "error": "",
        }
        return render(request, "login.html", context)


def salir(request):
    logout(request)
    messages.success(request, "Ha sido desconectado con exito")
    return HttpResponseRedirect(reverse('inicio'))

#------------------------------------------------------------------------------------------
# about


def about(request):
    usuario = "%s" %(request.user)
    context = {
        "usuario":usuario,
    }
    return render(request, "about.html",context)
def inicio(request):

    if request.user.is_authenticated():

        usuario = "%s" %(request.user)
        context = {
            "usuario" : usuario,
            "form": "hola",
            "queryset":"hola2"
        }
        # if form.is_valid():
        #     form.save(commit = True)
        #     #instance = form.save(commit = True)
        #     #print instance.nombre
        #     #print instance.codigo
        #     nombre = form.cleaned_data.get("nombre")
        #     context = {
        #         "titulo_template":"gracias %s, ya se ha registrado" %(nombre)
        #     }



        return render(request, "inicio.html",context)
    else:
        return HttpResponseRedirect(reverse('login'))
#------------------------------------------------------------------------------------------
# los editar datos




def edit_art(request,codigo):
    art = Articulo.objects.get(codigo=codigo)
    print "hola1"
    if request.method == "POST":
        print "hola2"
        form = Articulo_edit_form(request.POST ,request.FILES)
        if form.is_valid():
            print "hola3"
            art.nombre = form.cleaned_data['nombre']
            art.marca = form.cleaned_data['marca']
            art.descripcion  = form.cleaned_data['descripcion']
            art.cantidad = form.cleaned_data['cantidad']
            art.user_update = str("%s" %(request.user))
            art.save()
            message = "se ha modificado el articulo: " + str(art.codigo)
            messages.success(request, message)
            return HttpResponseRedirect(reverse('l_articulo'))

    if request.method == 'GET':
        form = Articulo_edit_form(initial={
            'nombre':art.nombre,
            'marca':art.marca,
            'descripcion':art.descripcion,
            'cantidad':art.cantidad,
        })
    context = {'form':form,'articulo':art}
    return render_to_response('edit.html',context,context_instance=RequestContext(request))


def edit_solicitante(request,codigo):
    sol = Solicitante.objects.get(rut=codigo)
    if request.method == "POST":
        form = Solicitante_edit_form(request.POST ,request.FILES)
        if form.is_valid():
            sol.nombre = form.cleaned_data['nombre']
            sol.apellido = form.cleaned_data['apellido']
            sol.alias  = form.cleaned_data['alias']
            sol.area = form.cleaned_data['area']
            sol.telefono = form.cleaned_data['telefono']
            sol.save()
            message = "se ha modificado el solicitante: " + str(sol.rut)
            messages.success(request, message)
            return HttpResponseRedirect(reverse('l_solicitante'))

    if request.method == 'GET':
        form = Solicitante_edit_form(initial={
            'nombre':sol.nombre,
            'apellido':sol.apellido,
            'alias':sol.alias,
            'area':sol.area,
            'telefono':sol.telefono,
        })
    titulo = "Editar Solicitante: "+ str(codigo)
    context = {'form':form,'solicitante':sol ,'titulo':titulo}
    return render_to_response('edit.html',context,context_instance=RequestContext(request))



def devolver(request,codigo):
    prest = Prestamo.objects.get(id=codigo)
    prest.estado = False
    cod = str(prest.codigo_art).split("-")
    art = Articulo.objects.get(codigo=cod[0])
    art.en_prestamo = art.en_prestamo - prest.cantidad
    art.update_ingre = str("%s" %(request.user))
    art.save()
    prest.save()
    message = "Del articulo: " + str(prest.codigo_art) + " se han devuelto exitosamente :" + str(prest.cantidad)
    messages.success(request, message)
    return HttpResponseRedirect(reverse('l_prestamov'))

def edit_list_articulo(request):
    if request.user.is_authenticated():
        queryset = Articulo.objects.all()
        context = {
            "titulo": "Editar Articulo",
            "pos": "articulo",
            "queryset": queryset
        }

        return render(request, "list_edit.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

def edit_list_prestamo(request):
    if request.user.is_authenticated():
        queryset = Prestamo.objects.all()
        context = {
            "titulo": "Devolucion Prestamos",
            "pos": "prestamo",
            "queryset": queryset
        }

        return render(request, "list_edit.html",context)
    else:
        messages.success(request, "el usuario no esta conectado")
        return HttpResponseRedirect(reverse('inicio'))

#------------------------------------------------------------------------------------------
# los eliminar datos


def delete_solicitante(request,codigo):
    sol = Solicitante.objects.get(rut=codigo)
    sol.delete()
    titulo = "se ha eliminado Solicitante: "+ str(codigo)
    messages.success(request, titulo)
    return HttpResponseRedirect(reverse('l_solicitante'))
