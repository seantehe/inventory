from django import forms
from .models import Articulo
from .models import Solicitante
from .models import Prestamo

class Articulo_form(forms.ModelForm):
    class Meta:
        model = Articulo
        fields = ["codigo","nombre","marca","cantidad","descripcion"]
class Articulo_edit_form(forms.ModelForm):
    class Meta:
        model = Articulo
        fields = ["nombre","marca","cantidad","descripcion"]


class Solicitante_form(forms.ModelForm):
    class Meta:
        model = Solicitante
        fields = ["rut","nombre","apellido","alias","area","telefono"]

class Solicitante_edit_form(forms.ModelForm):
    class Meta:
        model = Solicitante
        fields = ["nombre","apellido","alias","area","telefono"]

class Prestamo_form(forms.ModelForm):
    class Meta:
        model = Prestamo
        fields = ["codigo_art","id_solicitante","cantidad","descripcion"]
class articulo(forms.ModelForm):
    class Meta:
        model = Articulo
        fields = ["codigo"]
class edit_prestamo_search(forms.ModelForm):
    class Meta:
        model = Prestamo
        fields = ["codigo_art","id_solicitante"]
