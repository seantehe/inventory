from __future__ import unicode_literals
from django.core.validators import MaxValueValidator

from django.db import models

# Create your models here.
class Articulo(models.Model):
    codigo = models.CharField(max_length=120, primary_key=True)
    nombre = models.CharField(max_length=120, blank = False, null=False)
    marca = models.CharField(max_length=120)
    descripcion = models.TextField()
    cantidad = models.PositiveIntegerField()
    en_prestamo = models.IntegerField()
    user_ingre = models.CharField(max_length=120)
    user_update = models.CharField(max_length=120)
    ingreso = models.DateTimeField(auto_now_add=True, auto_now = False)
    update = models.DateTimeField(auto_now_add=False, auto_now = True)

    def __unicode__(self):
        return str(self.codigo)+"-"+str(self.nombre)

class Solicitante(models.Model):
    rut = models.PositiveIntegerField(primary_key=True, validators=[MaxValueValidator(99999999)])
    #rut = models.CharField(max_length=10, primary_key=True)
    nombre = models.CharField(max_length=120, blank = False, null=False)
    apellido = models.CharField(max_length=120)
    alias = models.CharField(max_length=120)
    area = models.CharField(max_length=120)
    telefono = models.CharField(max_length=120, blank = False, null=False)
    def __unicode__(self):
        return str(self.rut)+"-"+str(self.nombre)

class Prestamo(models.Model):
    id = models.AutoField('ID', primary_key=True)
    codigo_art = models.ForeignKey(Articulo, on_delete = models.CASCADE)
    id_solicitante = models.ForeignKey(Solicitante, on_delete = models.CASCADE)
    descripcion = models.TextField()
    estado = models.BooleanField()
    cantidad = models.PositiveIntegerField()
    user_ingre = models.CharField(max_length=120)
    update_ingre = models.CharField(max_length=120)
    ingreso = models.DateTimeField(auto_now_add=True, auto_now = False)
    update = models.DateTimeField(auto_now_add=False, auto_now = True)

